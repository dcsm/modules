Contributing to Solidipes
=========================

Thank you for wanting to contribute to the Solidipes Package. Any help is very much appreciated !

Below we provide guidelines for various types of contributions.

Bug Reports and Feature Requests
--------------------------------

Bugs and feature requests should be reported on GitLab: https://gitlab.com/dcsm/solidipes/-/issues

When reporting a bug, please include the following information:

    - Your operating system name and version.
    - Any details about your local setup that might be helpful in troubleshooting.
    - Detailed steps to reproduce the bug.

Submitting Pull Requests
-------------

If you do not already have a development environment set up, you will probably find the developer documentation helpful.

    Before submitting a pull request, please make sure you agree to the license and have submitted a signed contributor license agreement.
    PRs should include a short, descriptive title. The titles will be used to compile changelogs for releases, so think about the title in that context.
    Small improvements need not reference an issue, but PRs that introduce larger changes or add new functionality should refer to an issue.
    Structure your commits in meaningful units, each with an understandable purpose and coherent commit message. For example, if your proposed changes contain a refactoring and a new feature, make two PRs.
    Format commit messages using the `Conventional Commits <https://www.conventionalcommits.org/>`_ style

If you implement a new feature you are encouraged to provide tests and documentation for it.

You can access the respective guidelines here:

1. :ref:`testing-guidelines`

2. :ref:`docu-guidelines`
