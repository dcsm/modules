Setting Up The Developer Environment
====================================

.. _getting-started-dev:

Getting development started
---------------------------

Solidipes is a project that was currently only tested on Linux. For this reason we recommend using Linux or a Linux Subsystem (ie. `WSL for Windows users <https://docs.microsoft.com/en-us/windows/wsl/install-win10>`_).  For this reason the installation instructions below are only for Linux.

Creating a development Virtual Environment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To separate this project from other ones you may have on your computer and avoid package conflicts, it is best to create an environment, using for example Anaconda or Built-in Python venv module.

- :ref:`create-conda-environment`

- :ref:`venv-env`


Once you have created your environment, you can proceed to install Solidipes.

Installing `solidipes` package for `developers`
-----------------------------------------------

If you intend to implement new features into the code (like implementing a new reader for a specific file format or a new type of report), you need to get the source code of Solidipes.

Dependencies
~~~~~~~~~~~~

* Python (3.8 minimum)
* make
* `Poetry <https://python-poetry.org/docs/#installation>`_

Step 1: Cloning the `solidipes` repository
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: bash

   git clone https://gitlab.com/solidipes/solidipes.git
   cd solidipes


Step 2 : Option 1: Installing Dependencies in developer mode using pip command directly
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Install Dependencies
^^^^^^^^^^^^^^^^^^^^
1. Navigate to the directory where ``solidipes`` source code is located::

      cd /path/to/solidipes

Install `solidipes`
^^^^^^^^^^^^^^^^^^^^
1. Install ``solidipes`` in development mode::

      pip install -e .

Step 2 : Option 2: Installing Dependencies in developer mode using the Makefile
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Install Dev Dependencies
^^^^^^^^^^^^^^^^^^^^^^^^
1. Navigate to the directory where ``solidipes`` source code is located::

      cd /path/to/solidipes

2. Install a python version (3.8 or more recent)::

- If you are using **anaconda**::

    conda install python=3.9 #or more recent

- If you are using a **venv** you already installed a python3 version when creating the environment

This step is necessary for the following command to work correctly.

Install `solidipes` in dev mode
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
To install `solidipes`, run the following command::

      make install

Step 3: Verifying the Installation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Run the ``solidipes`` command to ensure it's installed and working correctly::

      solidipes --help

2. You should see the help text for ``solidipes`` indicating that it's properly installed.



Step 4: Enable auto-completion
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you are using *bash* or *zsh* as your shell, you can enable TAB completion for the ``solidipes`` command by running::

    activate-global-python-argcomplete

You may need to run this command as ``sudo`` or with the ``--user`` flag.
::
    activate-global-python-argcomplete --user
    #or
    sudo activate-global-python-argcomplete
