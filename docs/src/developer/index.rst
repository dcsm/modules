.. Rainy Days documentation master file, created by
   sphinx-quickstart on Wed Nov  9 22:59:45 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _developer-guide:
Developer Guide (in CONSTRUCTION)
=================================

.. attention::
   IN CONSTRUCTION

Solidipes is a Python package that aids the processes of publishing and sharing research data in the field of computational solid mechanics.
As a developer of Solidipes, you can help us expand its functionality and make it more user-friendly.
You can contribute to Solidipes by:

- adding new Loader types
- adding new Viewer types
- improving the existing code
- adding new features
- adding documentation
- testing

IN CONSTRUCTION

It is created within the scope of the `DCSM project <https://dcsm.readthedocs.io/>`_ (Dissemination of Computational Solid Mechanics).


.. toctree::
   :maxdepth: 2
   :caption: Developer Contents

   contribution_guidelines/contributing.rst
   dev_env_setup/dev_env_setup.rst
   code_overview/code_overview.rst
   API Reference <../../auto_source/solidipes/modules.rst>
   how_to_guides/how_to_guides
