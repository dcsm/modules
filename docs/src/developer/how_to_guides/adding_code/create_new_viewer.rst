Create new viewer type
----------------------

IN CONSTRUCTION

You can find in REFERENCEEEEEEEEEEE!!!!!! the list of all currently supported viewer types.

This guide will follow you through the process of creating a new viewer type.

If you want to add a new loader type, you need to:

1. create a newviewer.py file in the loaders folder (``/viewers``):

In our example: ``/viewers/newviewer.py``

2. create a new class in the newloader.py file, with the following structure::

    class NewViewer(Viewer):
        """ New viewer class """

        def __init__(self, data = None):
            """ Initialize the new viewer class """
            self.compatible_data_types = [loaders.NewLoader, # list of compatible data types
                                          loaders.NewLoader2,
                                          loaders.NewLoader3]
            #: NewViewer to display
            self.newviewertype = "NewViewer"
            super().__init__(data)

        def add(self, data_container, **kwargs):
            """ Add data to the viewer

            Args:
                **kwargs: keyword arguments passed to another function
            """

            self.check_data_compatibility(data_container)

            if isinstance(data_container, loaders.DataContainer):
                #self.newviewertype += data_container.newviewertype

            #elif isinstance(data_container, str):
            #    self.newviewertype += data_container


        def show(self):
            """ Show the data """
            if viewer_backends.current_backend == "jupyter notebook":

        def save(self, path, **kwargs):
            """Save the view to a file

            Args:
                path (str): path to the file
                **kwargs: keyword arguments passed to another function
            """




3. add the new viewer type to the ``__init__.py`` file in the viewers folder (``/viewers/__init__.py``)
::
    from .newviewer import NewViewer

and add the keyword "NewViewer" to the list of supported viewers::

    __all__ = ["File", .... , "NewViewer"]
