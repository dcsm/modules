How-To-Guides
=============

Here are simple guides that can help you contribute to the Solidipes package.

.. toctree::
   :maxdepth: 2
   :caption: Contents

   adding_code/create_new_loader.rst
   adding_code/create_new_viewer.rst
   contributing_to_documentation/contributing_to_documentation.rst
   testing/testing_guidelines.rst
