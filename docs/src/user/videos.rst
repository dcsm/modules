.. _video_doc:

Documentation in Videos
=======================

Introduction
^^^^^^^^^^^^

.. raw:: html

	 <video width="100%" controls>
	 <source src="https://gitlab.renkulab.io/guillaume.anciaux/solidipes-documentation-videos/-/raw/master/data/final_videos/solidipes-intro.mp4?ref_type=heads&inline=false" type="video/mp4"> Your browser does not support the video tag.</video>

Solidipes installation
^^^^^^^^^^^^^^^^^^^^^^

.. raw:: html

	 <video width="100%" controls>
	 <source src="https://gitlab.renkulab.io/guillaume.anciaux/solidipes-documentation-videos/-/raw/master/data/final_videos/solidipes-installation.mp4?ref_type=heads&inline=false" type="video/mp4"> Your browser does not support the video tag.</video>


Initialize a Solidipes dataset (from the terminal)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. raw:: html

	 <video width="100%" controls>
	 <source src="https://gitlab.renkulab.io/guillaume.anciaux/solidipes-documentation-videos/-/raw/master/data/final_videos/solidipes-initialise.mp4?ref_type=heads&inline=false" type="video/mp4"> Your browser does not support the video tag.</video>

From the terminal
^^^^^^^^^^^^^^^^^

Curation
--------

.. raw:: html

	 <video width="100%" controls>
	 <source src="https://gitlab.renkulab.io/guillaume.anciaux/solidipes-documentation-videos/-/raw/master/data/final_videos/solidipes-terminal-curation.mp4?ref_type=heads&inline=false" type="video/mp4"> Your browser does not support the video tag.</video>

Importing a Zenodo dataset
--------------------------

.. raw:: html

	 <video width="100%" controls>
	 <source src="https://gitlab.renkulab.io/guillaume.anciaux/solidipes-documentation-videos/-/raw/master/data/final_videos/solidipes-terminal-download-zenodo.mp4?ref_type=heads&inline=false" type="video/mp4"> Your browser does not support the video tag.</video>

Exporting a dataset to Zenodo
-----------------------------

.. raw:: html

	 <video width="100%" controls>
	 <source src="https://gitlab.renkulab.io/guillaume.anciaux/solidipes-documentation-videos/-/raw/master/data/final_videos/solidipes-terminal-export.mp4?ref_type=heads&inline=false" type="video/mp4"> Your browser does not support the video tag.</video>

Solidipes' web interface
^^^^^^^^^^^^^^^^^^^^^^^^

Overview of Solidipes' web interface
------------------------------------

.. raw:: html

	 <video width="100%" controls>
	 <source src="https://gitlab.renkulab.io/guillaume.anciaux/solidipes-documentation-videos/-/raw/master/data/final_videos/solidipes-web-overview.mp4?ref_type=heads&inline=false" type="video/mp4"> Your browser does not support the video tag.</video>


Import files to a dataset from Solidipes' web interface
-------------------------------------------------------

.. raw:: html

	 <video width="100%" controls>
	 <source src="https://gitlab.renkulab.io/guillaume.anciaux/solidipes-documentation-videos/-/raw/master/data/final_videos/solidipes-web-acquisition.mp4?ref_type=heads&inline=false" type="video/mp4"> Your browser does not support the video tag.</video>

Proceed to curation from Solidipes' web interface
-------------------------------------------------

.. raw:: html

	 <video width="100%" controls>
	 <source src="https://gitlab.renkulab.io/guillaume.anciaux/solidipes-documentation-videos/-/raw/master/data/final_videos/solidipes-web-curation.mp4?ref_type=heads&inline=false" type="video/mp4"> Your browser does not support the video tag.</video>

Export a dataset to Zenodo from Solidipes' web interface
--------------------------------------------------------

.. raw:: html

	 <video width="100%" controls>
	 <source src="https://gitlab.renkulab.io/guillaume.anciaux/solidipes-documentation-videos/-/raw/master/data/final_videos/solidipes-web-export.mp4?ref_type=heads&inline=false" type="video/mp4"> Your browser does not support the video tag.</video>

Annotate a dataset with metadata from Solidipes' web interface
--------------------------------------------------------------

.. raw:: html

	 <video width="100%" controls>
	 <source src="https://gitlab.renkulab.io/guillaume.anciaux/solidipes-documentation-videos/-/raw/master/data/final_videos/solidipes-web-metadata.mp4?ref_type=heads&inline=false" type="video/mp4"> Your browser does not support the video tag.</video>

Manage datasets from the online web service DCSM
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. raw:: html

	 <video width="100%" controls>
	 <source src="https://gitlab.renkulab.io/guillaume.anciaux/solidipes-documentation-videos/-/raw/master/data/final_videos/dcsm.mp4?ref_type=heads&inline=false" type="video/mp4"> Your browser does not support the video tag.</video>
