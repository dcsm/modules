.. _faq:
Frequently Asked Questions
==========================

What is solidipes for?
-----------------------

It is a user interface that allows to visualize and upload large data sets for researchers by creating a webreport.

Solidipes curates the data and signals errors in extension incompatibilities, missing headers etc.

The project can then be uploaded onto Zenodo, where other researchers can download the data with the relevant information associated to the data collection.


Is solidipes only available for Linux?
--------------------------------------

It is recommended to use Linux, as other OS have not yet been tested.

In later stages it might get tested for Windows and MacOS.

Is there a upload size limit?
-----------------------------

`Zenodo <https://zenodo.org/>`_ limits the data set size to 50GB.

What should I do when my data-type is not yet supported by solidipes?
---------------------------------------------------------------------

By default Solidipes will load and visualize your data set as if it were coded in binary. This will show as an error for the curation process.

..
    TO DO: Does it really raise an error? Or does it simply load it with binary and be happy??? CHECK WHEN WEBREPORT RUNS AGAIN

The rest of your data will still go through the curation process.

Otherwise, if the unsupported data-type is your only data-type, curation with Solidipes will not help you, you can directly upload it onto Zenodo.

If you want to help us develop the loaders for your data type, you can check out the :ref:`Developer Section<developer-guide>`.
