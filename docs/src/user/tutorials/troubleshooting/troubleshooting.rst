Troubleshooting
================

Loader Type not recognized:
------------------------------

This means that your data requires a different loader type that is not recognized. Currently only a limited number of loaders are supported.  See the config file for a list of supported loaders. If the necessary loader type is not supported the data is loaded with a default loader (binary). This may result in a loss of information.
If your loader type is not supported you can add it to the list of supported loaders by adding a new class to the loaders package. You can do this by downloading the package as a developer. See the `developer <../../../developer/index.html>`_ section for more information.

Viewer Type not recognized:
------------------------------

This means that your data requires a different viewer type that is not recognized. Currently only a limited number of viewers are supported.  See the config file for a list of supported viewers.
If your viewer type is not supported you can add it to the list of supported viewers by adding a new class to the viewers package.  You can do this by downloading the package as a developer. See the `developer <../../../developer/index.html>`_ section for more information.

Common errors:
----------------
::
    upload errorError updating deposition metadata: 400 Validation error.

    metadata.related_identifiers.0.identifier: Not a valid persistent identifier. Identifier is required.

This error is raised because the metadata file does not contain a valid identifier for the related identifier.  The identifier must be a valid DOI or ARK.  If you do not have a DOI or ARK for your data you can use the DOI minting service to create one.

To solve this issue, you must either remove the incomplete additional relation in the ``additional relations`` part of the webreport.
Or add an identifier to the ``identifier`` field like so:

.. image:: additional_relation.png
    :width: 800
