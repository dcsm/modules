import os
from pathlib import Path

import pytest
import utils

import solidipes as sp

# Imported fixtures
# - study_dir


@pytest.fixture
def text_file(study_dir):
    file_path = utils.get_asset_path("text.txt")
    return sp.load_file(file_path)


def test_load_invalid_path(study_dir):
    file_path = "invalid_path"

    with pytest.raises(FileNotFoundError):
        sp.load_file(file_path)

    with pytest.raises(RuntimeError):
        sp.loaders.File()


def test_load_without_extension(study_dir):
    file_path = utils.get_asset_path("image")
    file = sp.load_file(file_path)
    assert isinstance(file, sp.loaders.Image)
    assert file.file_info.type == "image/jpeg"
    assert file.valid_loading is False


def test_load_binary(study_dir):
    file_path = utils.get_asset_path("binary.bin")
    file = sp.load_file(file_path)
    assert isinstance(file, sp.loaders.Binary)
    assert file.file_info.size == 100
    assert type(file.text) is str
    assert len(file.text) != 0
    # assert file.valid_loading


def test_load_code_snippet(study_dir):
    file_path = utils.get_asset_path("code.py")
    file = sp.load_file(file_path)
    assert isinstance(file, sp.loaders.CodeSnippet)
    assert "Hello World!" in file.text
    assert file.valid_loading


def test_load_image_with_exif(study_dir):
    file_path = utils.get_asset_path("image_with_exif.jpg")
    file = sp.load_file(file_path)
    assert isinstance(file, sp.loaders.Image)
    assert file.file_info.type == "image/jpeg"
    assert "ExifVersion" in file.exif_data._data_collection
    assert file.valid_loading


def test_load_pdf(study_dir):
    file_path = utils.get_asset_path("document.pdf")
    file = sp.load_file(file_path)
    assert isinstance(file, sp.loaders.PDF)
    assert file.file_info.type == "application/pdf"
    assert file.pdf is not None
    assert file.valid_loading


class TestLoadTable:
    def test_csv(self, study_dir):
        file_path = utils.get_asset_path("table.csv")
        file = sp.load_file(file_path)
        assert isinstance(file, sp.loaders.Table)
        assert file.header == "a, b, c"
        assert file.table is not None
        assert file.valid_loading

    def test_excel(self, study_dir):
        file_path = utils.get_asset_path("table.xlsx")
        file = sp.load_file(file_path)
        assert isinstance(file, sp.loaders.Table)
        assert file.header == "a, b, c"
        assert file.table is not None
        assert file.valid_loading


def test_load_symlink(study_dir):
    file_path = utils.get_asset_path("symlink.txt")
    file = sp.load_file(file_path)
    assert isinstance(file, sp.loaders.SymLink)
    assert isinstance(file.linked_file, sp.loaders.Text)
    assert file.linked_file.text == "Hello World!\n"
    assert file.valid_loading


def test_load_text(text_file):
    assert isinstance(text_file, sp.loaders.Text)
    assert text_file.text == "Hello World!\n"
    assert text_file.valid_loading


def test_load_video(study_dir):
    file_path = utils.get_asset_path("video.mp4")
    file = sp.load_file(file_path)
    assert isinstance(file, sp.loaders.Video)
    assert file.file_info.type == "video/mp4"
    assert file.video is not None
    assert file.valid_loading


class TestDataContainerMethods:
    def test_data_info(self, text_file):
        """Test data_info method"""

        data_info = text_file.data_info
        assert type(data_info) is str
        assert len(data_info) != 0
        assert text_file.valid_loading

    def test_data(self, text_file):
        """Test data method"""

        data = text_file.data
        assert type(data) is dict
        # Test that accessing ".data" loads loadables
        assert data["text"] == "Hello World!\n"
        assert text_file.valid_loading


def test_file_sequence_detection() -> None:
    # Test node without sequence
    is_dir_path_dict = {
        "file1.txt": False,
        "other1.txt": False,
        "directory1": True,
        "directory2": True,
    }
    groups = sp.loaders.FileSequence._find_groups(is_dir_path_dict)
    assert groups == {}

    # Test node with sequences
    is_dir_path_dict = {
        "file1.txt": False,
        "file2.txt": False,
        "file3.txt": False,
        "other1.txt": False,
        "other2.txt": False,
        "other10.txt": False,
        "directory1": True,
        "directory2": True,
    }
    groups = sp.loaders.FileSequence._find_groups(is_dir_path_dict)
    assert groups == {
        "file*.txt": ["file1.txt", "file2.txt", "file3.txt"],
        "other*.txt": ["other1.txt", "other2.txt", "other10.txt"],
    }


def test_load_file_sequence(study_tree: Path) -> None:
    # Test node without sequence
    node = study_tree / "data"
    names = os.listdir(study_tree)
    is_dir_path_dict = {name: os.path.isdir(name) for name in names}
    root_path = str(node)

    loaded_groups, remaining_is_dir_path_dict = sp.loaders.load_groups(is_dir_path_dict, root_path)

    assert loaded_groups == {}
    assert remaining_is_dir_path_dict == is_dir_path_dict

    # Test node with sequence
    node = study_tree / "data" / "subdir1"
    names = os.listdir(node)
    is_dir_path_dict = {name: os.path.isdir(name) for name in names}
    root_path = str(node)

    loaded_groups, remaining_is_dir_path_dict = sp.loaders.load_groups(is_dir_path_dict, root_path)

    assert len(loaded_groups) == 1
    assert "file*.txt" in loaded_groups
    assert isinstance(loaded_groups["file*.txt"], sp.loaders.FileSequence)
    assert remaining_is_dir_path_dict == {
        "other.txt": False,
    }
