import pandas as pd
import pyvista as pv
from PIL import Image


def main():
    generate_binary_assets()
    generate_text_assets()
    generate_table_assets()
    generate_pyvista_assets()


def generate_binary_assets():
    generate_binary_file()
    generate_image_without_extension()


def generate_binary_file():
    """Generate a binary file with zeros"""
    file_size = 100
    with open("binary.bin", "wb") as f:
        f.write(b"\0" * file_size)


def generate_image_without_extension():
    """Generate a blank image of 1x1 pixels and export it in jpg"""
    image = Image.new("RGB", (1, 1))
    image.save("image", "JPEG")


def generate_text_assets():
    text = "Hello World!\n"
    with open("text.txt", "w") as f:
        f.write(text)


def generate_table_assets():
    df = pd.DataFrame({
        "a": [1, 4],
        "b": [2, 5],
        "c": [3, 6],
    })
    df.to_csv("table.csv", index=False)
    df.to_excel("table.xlsx", index=False)


def generate_pyvista_assets():
    generate_vtu()
    generate_pyvista_plot()


def generate_vtu():
    points = [[0.0, 0.0, 0.0], [1.0, 0.0, 0.0], [0.0, 1.0, 0.0]]
    cells = [3, 0, 1, 2]
    cell_types = [pv.CellType.TRIANGLE]

    mesh = pv.UnstructuredGrid(cells, cell_types, points)
    mesh.point_data.set_array([1, 2, 3], "point 1D")
    mesh.point_data.set_array(pv.pyvista_ndarray([[1, 2, 3], [4, 5, 6], [7, 8, 9]]), "point 3D")
    mesh.cell_data.set_array([1], "cell 1D")
    mesh.cell_data.set_array(pv.pyvista_ndarray([[1, 2, 3]]), "cell 3D")

    mesh.save("pyvista_mesh.vtu")


def generate_pyvista_plot():
    mesh = pv.read("pyvista_mesh.vtu")
    plotter = pv.Plotter(off_screen=True)
    plotter.background_color = "black"
    plotter.add_mesh(mesh)
    plotter.screenshot("pyvista_plot.png", window_size=(50, 50))


if __name__ == "__main__":
    main()
