# Simple example

## Run in terminal

```
poetry run python3 example.py
```


## In Jupyter notebook

Run
```
poetry run jupyter-notebook
```
and open `example.ipynb`


## With Streamlit

```
poetry run streamlit run example.py
```
